# Calendar QML components

## Screenshots

WIP

## Build

```
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=~/.local/kde -GNinja
ninja
```

## License

This project is licensed under LGPL-2.1-or-later. Some files are licensed under
more permissive licenses.
